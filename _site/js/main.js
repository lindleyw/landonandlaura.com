function postToGoogle() {
                var attending = $("input[type='radio'][name='attending_yes']:checked").val() || $("input[type='radio'][name='attending_no']:checked").val() ;
                var numberGuests = $("input[type='text'][name='numberGuests']").val();
                var song1 = function(){
                    var song_name;
                    var val = $("#entry_4580497").val();
                    var xVal = $("#entry_4580497").attr('initial-value');

                    if(val == xVal){
                        song_name = '';
                    } else{
                        song_name = val;

                    }
                    return song_name;

                } 

                
                var message = $("textarea[name='message']").val();
                var name = $("input[type='text'][name='name']").val();
                var email = $("input[type='text'][name='email']").val();
                var phone = $("input[type='text'][name='phone']").val();

                $.ajax({
                    url: "https://docs.google.com/forms/d/1doPN328N1DFCIrnrWK51NJOgIEO7yAzQ5hiWcxkU2Zw/formResponse",
                    data: {"entry.105682394": attending, "entry.1845636096": numberGuests, "entry.4580497": song1(), "entry.981460871": message, "entry.301279535": name, "entry.405641733": email, "entry.1206263867": phone   },
                    type: "POST",
                    dataType: "xml",
                    statusCode: {
                        0: function() {
                            //Success message
                        },
                        200: function() {
                            
                        }
                    }
                });
 
                
            }
             
$(document).ready(function(){
 

    $.validate({
      validateOnBlur: false, // disable validation when input looses focus
      errorMessagePosition : 'top', // Instead of 'element' which is default
      form: '#form',
    
     onSuccess : function(){
        postToGoogle();
        $('#form').hide();
         $('#success').show();
         return false; // Will stop the submission of the form




     }

     });



      $("#slides").slidesjs({
        width: 1100,
        height: 733,
        navigation: false,
        play: {
             active: true,             
             effect: "slide",             
             interval: 5000,              
             auto: true,             
             swap: true,             
             pauseOnHover: false,             
             restartDelay: 2500
            }
      });

     
});